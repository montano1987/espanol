<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class Profile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "user_id" => $this->id,
            "uuid" => $this->uuid,
            "email" => $this->email,
            "bio" => isset($this->resource->profile)? $this->resource->profile->bio : null,
            "name" => isset($this->resource->profile)? $this->resource->profile->name : null,
            "lastname" => isset($this->resource->profile)? $this->resource->profile->lastname : null,
            "avatar" => isset($this->resource->profile)? $this->resource->profile->avatar_url : null,
            "state" => isset($this->resource->profile)? $this->resource->profile->state : null,
            "cellphone" => isset($this->resource->profile)? $this->resource->profile->cellphone : null,
            "compannia" => isset($this->resource->profile)? $this->resource->profile->compannia : null,
            "compannia_name" => isset($this->resource->profile)? $this->resource->profile->compannia_name : null,
            "comercial_name_compannia" => isset($this->resource->profile)? $this->resource->profile->comercial_name_compannia : null,
            "address" => isset($this->resource->profile)? $this->resource->profile->address : null,
            "tax_id_number" => isset($this->resource->profile)? $this->resource->profile->tax_id_number : null,
            "web_site" => isset($this->resource->profile)? $this->resource->profile->web_site : null,
//            "profile" => $this->resource->profile,
        ];
//        return parent::toArray($request);
    }
}
