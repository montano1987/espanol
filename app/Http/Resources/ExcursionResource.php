<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ExcursionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);
        $result[ "imgeUrl" ] =    Storage::disk("public")->url("excursiones/".$this->image);
        if ($this->video!="")
        {
            $result[ "videoUrl" ] =    Storage::disk("public")->url("excursiones/".$this->video);
        }
        return $result;
    }
}
