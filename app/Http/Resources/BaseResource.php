<?php

namespace App\Http\Resources;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BaseResource 
{
    public function commonGetAllWithState($model, $state)
    {
        try {
           $rows = DB::transaction(function () use ($model, $state) {
                $row = [];
                if($state){
                    $row = $model::where('state', 1)->get();
                }else{
                    $row = $model::get();
                }
                return $row;
            });
            return [
                'data' => $rows,
                'code' => 200
            ];
        } catch (Exception $ex) {
            return [
                'data' => $ex->getMessage(),
                'code' => 500
            ];
        }
    } 
    
    public function commonGetOne($model, $id)
    {
        try {
            $row = DB::transaction(function () use ($model, $id) {
                $row = $model::find($id);
                return $row;
            });
            return [
                'data' => $row,
                'code' => 200
            ];
        } catch (Exception $ex) {
            return [
                'data' => $ex->getMessage(),
                'code' => 500
            ];
        }
    }
    
    public function commonCreate($model, $request)
    {
        try {
            $row = DB::transaction(function () use ($model, $request) {
                $row = $model::create($request->all());
                return $row;
            });
            return [
              'data' => $row,
              'code' => 200
            ];
        } catch (Exception $ex) {
            return [
                'data' => $ex->getMessage(),
                'code' => 500
            ];
        }
    }
    
    public function commonEdit($model, $request)
    {
        try {
            $row = DB::transaction(function () use ($model, $request) {
                $row = $model::where('id', $request->id)->update($request->all());
                return $row;
            });
            return [
                'data' => $row,
                'code' => 200
            ];
        } catch (Exception $ex) {
            return [
                'data' => $ex->getMessage(),
                'code' => 500
            ];
        }
    }
    
    public function commonDeleteOne($model, $id)
    {
        try {
            $row = DB::transaction(function () use ($model, $id) {
                $row = $model::findOrFail($id);
                $row::where('id', $id)->delete();
                return $row;
            });
            return [
                'data' => $row,
                'code' => 200
            ];
        } catch (Exception $ex) {
            return [
                'data' => $ex->getMessage(),
                'code' => 500
            ];
        }
    }
}