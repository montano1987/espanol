<?php

namespace App\Http\Controllers\API;

use App\Models\Excursion;
use App\Models\ExcursionDay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExcursionDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Excursion $excursion)
    {
        return response()->json($excursion->excursionday());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Excursion $excursion, Request $request)
    {
        // Hace las validaciones de los datos enviados por el request
        $request->validate([
            'title'                   => 'required|string',
            'subtitle'                   => 'required|string',
            'description'                   => 'required|string',
        ]);
        $excusionday = $excursion->excursionday()->create($request->all());
        return response()->json($excusionday);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExcursionDay  $excursionDay
     * @return \Illuminate\Http\Response
     */

    public function show(Excursion $excursion, $excursionday_id)
    {
        $excursionday = $excursion->excursionday()->findOrFail($excursionday_id);
        return response()->json($excursionday);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExcursionDay  $excursionDay
     * @return \Illuminate\Http\Response
     */
    public function edit(ExcursionDay $excursionDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExcursionDay  $excursionDay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  Excursion $excursion, $excursionday_id)
    {
        //
        // Hace las validaciones de los datos enviados por el request
        $request->validate([
            'title'                   => 'required|string',
            'subtitle'                   => 'required|string',
            'description'                   => 'required|string',
        ]);

        $excursionday = $excursion->excursionday()->findOrFail($excursionday_id);
        $excursionday->update($request->all());
        return response()->json($excursionday);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExcursionDay  $excursionDay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Excursion $excursion, $excursionday_id)
    {
        $excursionday = $excursion->excursionday()->findOrFail($excursionday_id);
        return response()->json($excursionday->delete());
    }
}
