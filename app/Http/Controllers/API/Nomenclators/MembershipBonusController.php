<?php

namespace App\Http\Controllers\API\Nomenclators;

use App\Http\Resources\BaseResource;
use App\Models\Nomenclators\MembershipBonus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Validator;

class MembershipBonusController extends Controller
{
    //
    public function __construct(BaseResource $baseResource)
    {
        $this->baseResource = $baseResource;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rows = $this->baseResource->commonGetAllWithState(MembershipBonus::class,null);
            return $rows;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:nom_membership_bonuses|required',
                'descriptions' => 'unique:nom_membership_bonuses|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonCreate(MembershipBonus::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nomenclators\MembershipBonus  $membershipBonus
     * @return \Illuminate\Http\Response
     */
    public function show(MembershipBonus $membershipBonus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nomenclators\MembershipBonus  $membershipBonus
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:nom_membership_bonuses,name,' . $request->id,
                'description' => 'required|unique:nom_membership_bonuses,description,' . $request->id,
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonEdit(MembershipBonus::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonDeleteOne(MembershipBonus::class,$request->id);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
}
