<?php

namespace App\Http\Controllers\Nomenclators;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseResource;
use App\Models\Nomenclators\NomLanguage;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Validator;

class LanguagesController extends Controller
{
    //
    public function __construct(BaseResource $baseResource)
    {
        $this->baseResource = $baseResource;
    }

    public function index(Request $request)
    {
        try {
            $rows = $this->baseResource->commonGetAllWithState(NomLanguage::class,null);
            return $rows;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:nom_languages|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonCreate(NomLanguage::class,$request);
            return $row;
        } catch (Exception $ex) {
             return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function edit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:nom_languages,name,' . $request->id,
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonEdit(NomCurrency::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonDeleteOne(NomLanguage::class,$request->id);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
}
