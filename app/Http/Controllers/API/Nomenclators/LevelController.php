<?php

namespace App\Http\Controllers\API\Nomenclators;

use App\Models\Nomenclators\CicleType;
use App\Models\Nomenclators\Level;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BaseResource;
use Exception;
use Illuminate\Support\Facades\Validator;

class LevelController extends Controller
{
    public function __construct(BaseResource $baseResource)
    {
        $this->baseResource = $baseResource;
    }
    public function index(Request $request)
    {
        try {
            $rows = $this->baseResource->commonGetAllWithState(CicleType::class,null);
            return $rows;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:levels|required',
                'descriptions' => 'unique:levels|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonCreate(Level::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nomenclators\MembershipBonus  $membershipBonus
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:levels,name,' . $request->id,
                'description' => 'required|unique:levels,description,' . $request->id,
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonEdit(Level::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonDeleteOne(Level::class,$request->id);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
}
