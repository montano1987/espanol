<?php

namespace App\Http\Controllers\API\Nomenclators;

use App\Models\Nomenclators\PromotionBenefit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BaseResource;
use Exception;
use Illuminate\Support\Facades\Validator;


class PromotionBenefitController extends Controller
{
    //
    public function __construct(BaseResource $baseResource)
    {
        $this->baseResource = $baseResource;
    }
    public function index(Request $request)
    {
        try {
            $rows = $this->baseResource->commonGetAllWithState(PromotionBenefit::class,null);
            return $rows;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:promotion_benefits|required',
                'descriptions' => 'unique:promotion_benefits|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonCreate(PromotionBenefit::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
    public function edit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:promotion_benefits,name,' . $request->id,
                'description' => 'required|unique:promotion_benefits,description,' . $request->id,
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonEdit(PromotionBenefit::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonDeleteOne(PromotionBenefit::class,$request->id);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

}
