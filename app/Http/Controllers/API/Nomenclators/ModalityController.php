<?php

namespace App\Http\Controllers\API\Nomenclators;

use App\Models\Nomenclators\Modality;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BaseResource;
use Exception;
use Illuminate\Support\Facades\Validator;

class ModalityController extends Controller
{
    //
    //
    public function __construct(BaseResource $baseResource)
    {
        $this->baseResource = $baseResource;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:modalities|required',
                'descriptions' => 'unique:modalities|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonCreate(Modality::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nomenclators\MembershipBonus  $membershipBonus
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:modalities,name,' . $request->id,
                'description' => 'required|unique:modalities,description,' . $request->id,
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonEdit(Modality::class,$request);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }

    public function delete(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                return [
                    'data' => 'UniqueValue',
                    'code' => 501
                ];
            }
            $row = $this->baseResource->commonDeleteOne(Modality::class,$request->id);
            return $row;
        } catch (Exception $ex) {
            return [
                'data' => 'UniqueValue',
                'code' => 500
            ];
        }
    }
}
