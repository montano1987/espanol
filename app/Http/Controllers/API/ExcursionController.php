<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ExcursionResource;
use App\Models\Excursion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;

class ExcursionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return ExcursionResource::collection(Excursion::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Hace las validaciones de los datos enviados por el request
        $request->validate([

            'title'                  => 'required|string|max:250',
            'subtitle'               => 'required|string|max:250',
            'description'            => 'required',
            'min_person'              => 'required|integer',
            'price_person'           => 'required|numeric',
            'imageFile'                 => 'required|file',
            'country_id'             => 'required',
        ]);
        if ($request->hasFile('imageFile')) {
            $upload_file = $request->imageFile;
            $uuid = Uuid::uuid4();;
            $extension = $upload_file->extension();
            $nameFile = $uuid. '.' . $extension;
            $upload_file->storeAs("excursiones", $nameFile, "public");

            $nameFileVideo = "";
            if ($request->hasFile('video')){
                $upload_file_video = $request->video;
                $uuid_video = Uuid::uuid4();
                $extension_video = $upload_file_video->extension();
                $nameFileVideo = $uuid_video. '.' . $extension_video;
                $upload_file_video->storeAs("excursiones", $nameFileVideo, "public");
            }
            $excursion = Excursion::create(array_merge($request->all(),['image' => $nameFile,'video' => $nameFileVideo]));

            return  new ExcursionResource( $excursion );
        }
        abort(422,"fichero no valido");


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Excursion  $excursion
     * @return \Illuminate\Http\Response
     */
    public function show(Excursion $excursion)
    {
        return  new ExcursionResource( $excursion );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Excursion  $excursion
     * @return \Illuminate\Http\Response
     */
    public function edit(Excursion $excursion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Excursion  $excursion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Excursion $excursion)
    {

        // Hace las validaciones de los datos enviados por el request
        $request->validate([

            'title'                  => 'required|string|max:250',
            'subtitle'               => 'required|string|max:250',
            'description'            => 'required',
            'min_person'              => 'required|integer',
            'price_person'           => 'required|numeric',
            'country_id'             => 'required',
        ]);
        $nameFile=$excursion->image;
        if ($request->hasFile('imageFile') ) {
            $upload_file = $request->imageFile;
            $uuid = Uuid::uuid4();;
            $extension = $upload_file->extension();
            $nameFile = $uuid. '.' . $extension;
            $upload_file->storeAs("excursiones", $nameFile, "public");
        }
        $nameFileVideo = $excursion->video;
        if ($request->hasFile('video')){
            $upload_file_video = $request->video;
            $uuid_video = Uuid::uuid4();
            $extension_video = $upload_file_video->extension();
            $nameFileVideo = $uuid_video. '.' . $extension_video;
            $upload_file_video->storeAs("excursiones", $nameFileVideo, "public");
        }
        $excursion->update(array_merge($request->all(),['image' => $nameFile,'video' => $nameFileVideo]));

        return  new ExcursionResource( $excursion );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Excursion  $excursion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Excursion $excursion)
    {
        //
        if($excursion->delete())
        {
            return response( )->json([
                'success' => true,
                'message' => 'Excursion eliminada con éxito.',
                'code'    => 200,

            ], 200 );
        }
    }
}
