<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
