<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class CicleType extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
