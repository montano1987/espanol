<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class NomLanguage extends Model
{
    //
    protected $fillable = [
        'name',
    ];
}
