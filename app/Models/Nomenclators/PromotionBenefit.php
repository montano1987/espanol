<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class PromotionBenefit extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
