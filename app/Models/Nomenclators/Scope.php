<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class Scope extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
