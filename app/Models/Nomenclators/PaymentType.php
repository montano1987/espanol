<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
