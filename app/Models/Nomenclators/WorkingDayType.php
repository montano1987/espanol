<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class WorkingDayType extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
