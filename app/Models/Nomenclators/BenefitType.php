<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class BenefitType extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
