<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
