<?php

namespace App\Models\Nomenclators;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
    ];
}
