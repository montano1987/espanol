<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Excursion extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'min_person',
        'price_person',
        'image',
        'video',
        'country_id',
    ];

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'country_id');
    }

    public function programdates()
    {
        return $this->hasMany('App\Models\ProgramDate', 'excursion_id',
            'id');
    }
    public function excursionday()
    {
        return $this->hasMany('App\Models\ExcursionDay', 'excursion_id',
            'id');
    }
}
