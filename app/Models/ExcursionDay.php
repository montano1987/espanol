<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExcursionDay extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'excursion_id',
    ];
    public function excursion()
    {
        return $this->belongsTo('App\Models\Excursion', 'excursion_id');
    }
}
