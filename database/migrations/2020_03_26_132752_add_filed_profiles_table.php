<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            //
            $table->smallInteger('state')
                ->comment('Estado');
            $table->string('cellphone')->nullable()
                ->comment('Número de telefono');
            $table->boolean('compannia')
                ->comment('Compañia');
            $table->string('compannia_name')->nullable()
                ->comment('Nombre de la Compañía');
            $table->string('comercial_name_compannia')->nullable()
                ->comment('Nombre Comercial');
            $table->string('address')->nullable()
                ->comment('Dirección');
            $table->string('address')->nullable()
                ->comment('Dirección');
            $table->string('tax_id_number')->nullable()
                ->comment('Número de Identificación Fiscal');
            $table->string('web_site')->nullable()
                ->comment('Sitio Web');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            //
            $table->dropColumn('state');
            $table->dropColumn('cellphone');
            $table->dropColumn('compannia');
            $table->dropColumn('compannia_name');
            $table->dropColumn('comercial_name_compannia');
            $table->dropColumn('address');
            $table->dropColumn('tax_id_number');
            $table->dropColumn('web_site');
        });
    }
}
